
/*
 * (c) Copyright 2020 Brite:Bill Ltd.
 *
 * 7 Grand Canal Street Lower, Dublin 2, Ireland
 * info@britebill.com
 * +353 1 661 9426
 */
package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author <a href="mailto:razvanm@amdocs.com">Razvan Mihai</a>
 */
public class CalcmulTest {

    @Test
    public void testMul() {
        assertEquals(18, new Calcmul().mul());
    }
}
