
/*
 * (c) Copyright 2020 Brite:Bill Ltd.
 *
 * 7 Grand Canal Street Lower, Dublin 2, Ireland
 * info@britebill.com
 * +353 1 661 9426
 */
package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author <a href="mailto:razvanm@amdocs.com">Razvan Mihai</a>
 */
public class CntTest {

    @Test
    public void testA() {
        Cnt c = new Cnt();
        assertEquals(Integer.MAX_VALUE, c.d(1,0));
    }

    @Test
    public void testB() {
        Cnt c = new Cnt();
        assertEquals(2, c.d(2, 1));
    }
}
